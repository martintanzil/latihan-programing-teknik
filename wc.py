class WC:
    f = None
    namafile = ""

    def __init__(self, namafile):
        self.namafile = namafile

    def word_count(self):
        self.f = open(self.namafile, "r")
        words = 0 
        for line in self.f:
            words += len(line.split(" ")) # len = jumlah string dalm array 
        self.f.close()
        return words

    def line_count(self):
        self.f = open(self.namafile, "r")
        lines = 0 
        for line in self.f:
            lines += 1
        self.f.close()
        return lines

    def char_count(self):
        self.f = open(self.namafile, "r")
        chars = 0
        for line in self.f:
            for char in line:
                chars += 1
        self.f.close()
        return chars

        # if __name__ == '__main__':
        #     n = WC('Readme.md')
        #     print
